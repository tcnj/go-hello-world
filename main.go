package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("Hello World!")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Hello World!")
	})

	http.ListenAndServeTLS(":8080", "hello.crt", "hello.key", nil)
}